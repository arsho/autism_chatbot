Autism Chatbot
==============

![Autism Chatbot](screenshot/level1.png)

A chatbot to fulfill your query about autism.

### Initial Configuration

- Create a MySQL database. Name it "autism_db".
- Import the given [autism_db.sql](db/autism_db.sql)
- Update the database configuration in [db_operation.php](db_operation.php)		

### To Run the Application

- After setting up the initial configuration, just run the [index.php](index.php) file.

### What is this repository for?

- Quick summary
- Version Controlling
- Screenshot enlisting of each phase (See attached Screenshot folder)
- [Issue Tracking](https://bitbucket.org/arsho/autism_chatbot/issues)

## Acknowledgments

- Used Wit api [Wit official website](http://wit.ai/)
