<?php
require_once("db_operation.php");
$db = new DB_OPERATION;

if(isset($_POST["entity"])){
    global $db;
    $entity = $_POST["entity"];
    $results = $db->get_autism_answers_for_given_entity($entity);
    // TO DO:
    // results may have multiple answers
    // Choose one randomly
    echo $results[0]["answer"];
}
?>
