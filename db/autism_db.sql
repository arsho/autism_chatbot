-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2018 at 06:19 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autism_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer_table`
--

CREATE TABLE `answer_table` (
  `id` int(4) NOT NULL,
  `answer` varchar(1000) NOT NULL,
  `marked_entity` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_table`
--

INSERT INTO `answer_table` (`id`, `answer`, `marked_entity`) VALUES
(1, 'Autism is a life-long developmental disability that prevents individuals from properly understanding what they see, hear, and otherwise sense.', 'autism_introduction'),
(2, 'The degree of severity of characteristics differs from person to person, but usually includes: Severe delays in language development. Severe delays in understanding social relationships, Inconsistent Patterns of sensory responses, Uneven patterns of intellectual functioning, Marked restriction of activity and interests and so on.', 'autism_characteristics'),
(3, 'The cause of autism is still unknown. Some research suggests a physical problem affecting those parts of the brain that process language and information coming in from the senses. There may be some imbalance of certain chemicals in the brain.', 'autism_causes'),
(4, 'It occurs in 1 in every 110 births.', 'autism_statistics'),
(7, 'Individuals with autism have extreme difficulty in learning language and social skills and in relating to people.\r\n	\r\n', 'not_given'),
(8, 'autism is treatable. Studies show that all people who have autism can improve significantly with proper instruction. Many individuals with autism eventually become more responsive to others as they learn to understand the world around them.', 'not_given'),
(9, 'In general, individuals with autism perform best at jobs which are structured and involve a degree of repetition.', 'not_given'),
(10, 'Hello!', 'greetings'),
(11, 'Bye!', 'bye');

-- --------------------------------------------------------

--
-- Table structure for table `question_table`
--

CREATE TABLE `question_table` (
  `id` int(10) NOT NULL,
  `question` varchar(100) NOT NULL,
  `marked_entity` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_table`
--

INSERT INTO `question_table` (`id`, `question`, `marked_entity`) VALUES
(1, 'what is autism?', 'autism_introduction'),
(2, 'I want to know about autism', 'autism_introduction'),
(3, 'tell me about autism', 'autism_introduction'),
(4, 'what do you mean by autism?', 'autism_introduction'),
(5, 'what are the characteristics of autism?', 'autism_characteristics'),
(6, 'what are the symptoms of autism?', 'autism_characteristics'),
(7, 'give me a list of behaviors commonly seen in autism.', 'autism_characteristics'),
(8, 'what causes autism?', 'autism_causes'),
(9, 'reasons behind autism.', 'autism_causes'),
(10, 'what are the causes of autism? ', 'autism_causes'),
(11, 'how Common is autism?', 'autism_statistics'),
(12, 'what is the current autism rate all around the world?', 'autism_statistics'),
(13, 'give me the list of countries with the highest rates of autism.', 'autism_statistics');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer_table`
--
ALTER TABLE `answer_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_table`
--
ALTER TABLE `question_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer_table`
--
ALTER TABLE `answer_table`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `question_table`
--
ALTER TABLE `question_table`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
