<?php
    class DB_Operation{
        function __construct(){
            $username = "root";
            $password = "123";
            $host = "localhost";
            $dbname = "autism_db";
            $this->answer_table = "answer_table";
            try{
                $this->conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(Exception $e){
                echo "Error in Database connection: ";
            }
        }

        function get_autism_answers_for_given_entity($entity){
            try{
                $stmt = $this->conn->prepare("SELECT * FROM $this->answer_table WHERE marked_entity=:entity");
                $stmt->bindParam(':entity', $entity);
                $stmt->execute();
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $results = $stmt->fetchAll();
                return $results;
            }
            catch(Exception $e){
                //echo $e;
            }
        }
    }
?>
