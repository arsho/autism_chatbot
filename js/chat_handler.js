$(document).ready(function(){
    function append_to_div(div_id, message){
        $("#"+div_id).append(message+"<br/>");
    }

    function get_parsed_response(response){
        /*
            For each user message there can be multiple entities.
            Returns two dimensional array.
            [[entity, value, confidence], [entity, value, confidence]]
        */
        var information = [];
        var entities = response.entities;
        for(var key in entities){
            var value = "";
            var confidence = 0;
            var entity_values = entities[key];
            for(var i in entity_values){
                var current_confidence = entity_values[i].confidence;
                var current_value = entity_values[i].value;
                if(current_confidence>confidence){
                    confidence=current_confidence;
                    value=current_value;
                }
            }
            information.push([key, value, confidence]);
        }
        return information;
    }

    function reply_user(){
        $user_text = $("#user_text").val();
        $message = "User says: "+$user_text;
        append_to_div("result", $message);
        $("#user_text").val("");
        $.ajax({
            url: 'https://api.wit.ai/message',
            data: {
                'q': $user_text,
                'access_token' : 'QGIJVWTLH2KWZIUZON4UPO6BB3OU3KJH'
            },
            dataType: 'jsonp',
            method: 'GET',
            success: function(response) {
                information = get_parsed_response(response);
                number_of_entities = information.length;
                if(number_of_entities == 0){
                    $bot_reply = "Bot says: wubba lubba dub dub";
                    append_to_div("result", $bot_reply);
                }
                /* Bot will reply for each entity of user message */
                for(i=0; i<number_of_entities; i++){
                    current_information = information[i];
                    entity = current_information[0];
                    $.ajax({
                        url: '../api.php',
                        data: {
                            'entity': entity
                        },
                        method: 'POST',
                        success: function(database_result) {
                            $bot_reply = "Bot says: "+database_result;
                            append_to_div("result", $bot_reply);
                        }
                    });
                }
            }
        });
    }

    // Handle submit button click
    $("#chat_btn").on("click",function(){
        reply_user();
    });

    // Handle enter button click on textbox
    $("#user_text").on("keypress",function(e){
        if(e.which === 13){
            reply_user();
        }
    });
});
